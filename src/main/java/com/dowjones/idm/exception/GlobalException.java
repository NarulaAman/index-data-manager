package com.dowjones.idm.exception;

public interface GlobalException {

	public String getType();
	public String getMessage();
}
