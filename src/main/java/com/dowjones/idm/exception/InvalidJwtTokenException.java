package com.dowjones.idm.exception;

import org.springframework.security.core.AuthenticationException;

public class InvalidJwtTokenException extends AuthenticationException implements GlobalException{

	private static final String TYPE = "INVALID_ACCESS_TOKEN";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public InvalidJwtTokenException(String msg) {
		super(msg);
	}
	
	public String getType() {
		return TYPE;
	}

}
