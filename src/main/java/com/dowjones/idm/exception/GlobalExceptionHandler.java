package com.dowjones.idm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.dowjones.idm.rest.entity.Problem;

/**
 * @author amandeep
 *
 * REST API exception handler for http requests.
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(TickerNotFoundException.class)
	public ResponseEntity<Problem> handleIndexTickerNotFound(TickerNotFoundException ex, WebRequest request){
		return new ResponseEntity<Problem>(getProblem((DataException) ex , HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
	}
	
	/**
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(DataUploadException.class)
	public ResponseEntity<Problem> handleIndexTickerNotFound(DataUploadException ex, WebRequest request){
		return new ResponseEntity<Problem>(getProblem((DataException) ex , HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(InvalidCredentialsException.class)
	public ResponseEntity<Problem> handleInvalidJwtToken(InvalidCredentialsException ex, WebRequest request) {
		return new ResponseEntity<Problem>(getProblem((InvalidCredentialsException) ex, HttpStatus.FORBIDDEN), HttpStatus.FORBIDDEN);
	}

	/**
	 * @param ex
	 * @param status
	 * @return
	 */
	private Problem getProblem(GlobalException ex, HttpStatus status) {
		Problem problem = new Problem();
		problem.setStatusCd(status.value());
		problem.setType(ex.getType());
		problem.setTitle(ex.getType());
		problem.setDetail(ex.getMessage());
		return problem;
	}
	
	
	
	
}
