package com.dowjones.idm.exception;

public class InvalidCredentialsException extends RuntimeException implements GlobalException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TYPE = "INVALID_CREDENTIALS";

	
	public InvalidCredentialsException(String msg) {
		super(msg);
	}
	
	
	@Override
	public String getType() {
		return TYPE;
	}
}
