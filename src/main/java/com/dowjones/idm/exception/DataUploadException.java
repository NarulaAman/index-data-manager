package com.dowjones.idm.exception;

/**
 * @author amandeep
 *
 */
public class DataUploadException extends DataException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String ERROR_TYPE = "INDEX_DATA_UPLOAD_EXCEPTION";
	
	public DataUploadException(String msg) {
		super(ERROR_TYPE, msg);
	}
	
	
}
