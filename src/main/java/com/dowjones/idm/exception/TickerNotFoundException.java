package com.dowjones.idm.exception;

/**
 * @author amandeep
 *
 */
public class TickerNotFoundException extends DataException {

	private static final long serialVersionUID = 1L;
	private static final String ERROR_TYPE = "INDEX_TICKER_NOT_FOUND";
	
	public TickerNotFoundException(String msg) {
		super(ERROR_TYPE, msg);
	}

}
