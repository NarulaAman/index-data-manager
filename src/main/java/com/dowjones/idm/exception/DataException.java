package com.dowjones.idm.exception;

/**
 * @author amandeep
 *
 */
public abstract class DataException extends RuntimeException implements GlobalException  {

	private String exType;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataException(String msg) {
		super(msg);
	}
	
	public DataException(String type, String msg) {
		super(msg);
		exType = type;
	}
	
	public String getType() {
		return exType;
	}
	
	
}
