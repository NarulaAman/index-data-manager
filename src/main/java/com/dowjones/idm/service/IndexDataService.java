package com.dowjones.idm.service;

import com.dowjones.idm.rest.entity.IndexData;
import com.dowjones.idm.rest.entity.IndexDataSet;

/**
 * @author amandeep
 *
 */
public interface IndexDataService {

	/**
	 * @param dataSet
	 */
	public void uploadIndexDataSet(IndexDataSet dataSet);
	/**
	 * @param ticker
	 * @return
	 */
	public IndexDataSet getIndexDataByTicker(String ticker);
	/**
	 * @param record
	 */
	public void uploadIndexRecord(IndexData record);
}
