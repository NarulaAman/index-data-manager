package com.dowjones.idm.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dowjones.idm.exception.DataUploadException;
import com.dowjones.idm.exception.TickerNotFoundException;
import com.dowjones.idm.repository.IndexDataRepository;
import com.dowjones.idm.rest.entity.IndexData;
import com.dowjones.idm.rest.entity.IndexDataSet;

import lombok.extern.slf4j.Slf4j;

/**
 * @author amandeep
 *
 * Service class to manage the index data
 *
 */
@Service
@Slf4j
public class IndexDataServiceImpl implements IndexDataService {

	@Autowired
	private IndexDataRepository repository;


	/**
	 *
	 */
	public void uploadIndexDataSet(IndexDataSet dataSet) {
		try {
			repository.saveAll(dataSet.getDataSet()); 
			log.info("data set uploaded successfully.");
		} catch(Exception ex) {
			log.error("failed to upload data set - {}", ex.getMessage());
			throw new DataUploadException(ex.getMessage());
		}
	}

	/**
	 *
	 */
	public IndexDataSet getIndexDataByTicker(String ticker) {
		Collection<IndexData> indexDataList = repository.getIndexDataByTicker(ticker);
		IndexDataSet dataSet = new IndexDataSet();
		
		dataSet.setDataSet((List<IndexData>)indexDataList);
		
		if(indexDataList == null || indexDataList.isEmpty()) {
			log.error("Index ticker, {} not found", ticker);
			throw new TickerNotFoundException(String.format("Index ticker %s not found", ticker));
		}
		return dataSet;
	}

	/**
	 *
	 */
	public void uploadIndexRecord(IndexData record) {
		try {
			repository.save(record);
			log.info("record uploaded successfully.");
		} catch(Exception ex) {
			log.error("failed to upload record - {}", ex.getMessage());
			throw new DataUploadException(ex.getMessage());
		}
	} 

}
