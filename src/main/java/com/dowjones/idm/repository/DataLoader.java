package com.dowjones.idm.repository;

import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.dowjones.idm.rest.entity.IndexData;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;

/**
 * @author amandeep
 *
 * Data loader to parse csv index data.
 * 
 */
@Component
public class DataLoader {
	
	private static final String CSV_FILE_PATH = "/index-data/dow_jones_index.csv";

	@SuppressWarnings("deprecation")
	public List<IndexData> getIndexData() {

		HeaderColumnNameTranslateMappingStrategy<IndexData> strategy =
				new HeaderColumnNameTranslateMappingStrategy<IndexData>();
		strategy.setType(IndexData.class);
		strategy.setColumnMapping(getMapping());

		CSVReader csvReader = null;
		try {
			csvReader = new CSVReader(new InputStreamReader
					(getClass().getResourceAsStream(CSV_FILE_PATH)));
		}
		catch (RuntimeException e) {
			throw new RuntimeException(e);
		}
		CsvToBean<IndexData> csvToBean = new CsvToBean<>();

		List<IndexData> list = csvToBean.parse(strategy, csvReader);

		return list;
	}

	/**
	 * @return
	 */
	protected Map<String, String> getMapping() {
		Map<String, String> mapping = new 
				HashMap<String, String>();

		Field[] fields = IndexData.class.getDeclaredFields();
		for (Field field : fields) {
			String name = field.getName(); 
			mapping.put(name, name);
		}

		return mapping;
	}


}
