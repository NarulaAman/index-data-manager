package com.dowjones.idm.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.dowjones.idm.rest.entity.IndexData;

/**
 * @author amandeep
 *
 *  Repository for database operations.
 */
@Component
public interface IndexDataRepository extends JpaRepository<IndexData, Long> {
	
	 @Query(value = "SELECT * FROM IndexData t where t.stock = :stock", nativeQuery = true) 
	 Collection<IndexData> getIndexDataByTicker(@Param("stock") String stock);
	
}
