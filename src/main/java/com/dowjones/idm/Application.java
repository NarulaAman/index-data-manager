package com.dowjones.idm;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Bean;

import com.dowjones.idm.repository.DataLoader;
import com.dowjones.idm.repository.IndexDataRepository;

@SpringBootApplication(exclude = {UserDetailsServiceAutoConfiguration.class})
public class Application {

	@Autowired
	private DataLoader dataLoader;
	
	@Autowired
	private IndexDataRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Persist index data into database
	 * 
	 * @return
	 */
	@Bean
	InitializingBean intializeDatabase() {
		return () -> {
			repository.saveAll(dataLoader.getIndexData());
		};
	}
}
