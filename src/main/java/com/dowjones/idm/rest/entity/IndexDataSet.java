package com.dowjones.idm.rest.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class IndexDataSet {

	private List<IndexData> dataSet;
	
}
