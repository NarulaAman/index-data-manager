package com.dowjones.idm.rest.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * @author amandeep
 * 
 * Entity for relational mapping for index data
 * 
 *
 */
@Setter
@Getter
@Entity
@Table(name = "indexdata")
public class IndexData {

	/* 
	Note: Type is string for all fields as data provided was inconsistent
	To be Optimized
	*/
	
	@Id
	@GeneratedValue
	private Long id;
	
	private int quarter;
	private String stock;
	private String date;
	private String open;
	private String high;
	private String low;
	private String close;
	private String volume;
	private String percentChangePrice;
	private String percentChangeVolOvrLastWk;
	private String previousWkVol;
	private String nextWkOpen;
	private String nextWkClose;
	private String percentChangeNextWkPrice;
	private int daysDividend;
	private String percentReturnNextDividend;
	
}
