package com.dowjones.idm.rest.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author amandeep
 *
 */
@Setter
@Getter
public class Problem {

	private int statusCd;
	private String type;
	private String title;
	private String detail;
	
}
