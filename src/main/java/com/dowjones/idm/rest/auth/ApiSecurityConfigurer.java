package com.dowjones.idm.rest.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.dowjones.idm.rest.auth.jwt.JwtAuthenticationFilter;
import com.dowjones.idm.rest.auth.jwt.JwtAuthenticationProvider;

/**
 * @author amandeep
 *
 *
 * HTTP Security configurer 
 *
 */
@EnableWebSecurity(debug = true)
@Configuration
public class ApiSecurityConfigurer extends WebSecurityConfigurerAdapter {

	private static final String URL_PATTERN = "/dowjones-index/**";
	private static final String AUTH_URL_PATTERN = "/authenticate";
	
	@Autowired
	private JwtAuthenticationProvider provider;
	
	@Autowired
	private JwtAuthenticationFilter jwtFilter;
	
	/**
	 * Configures the authentication manager
	 *
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		
		// Credentials in real world scenario will be fetched from secured database (Not in memory)
		auth.inMemoryAuthentication()
		.withUser("admin")
		.password(encoder.encode("adminpwd"))
		.roles("admin_role"); 
		

		auth.authenticationProvider(provider);
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.antMatcher(URL_PATTERN)
		.csrf().disable()
		.sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		 // NOT to Create Session -- Setup security context each time
		
		http
		.authorizeRequests()
		.anyRequest()
		.authenticated()
		.and()
		.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Bean
	public UserDetailsService userDetailsService() {
	    return super.userDetailsService();
	}
	
	 @Override
	    public void configure(WebSecurity web) throws Exception {
	        web.ignoring()
	        .antMatchers(AUTH_URL_PATTERN);
	    }

	 @Bean
	 public FilterRegistrationBean<JwtAuthenticationFilter> preAuthTenantContextInitializerFilterRegistration(JwtAuthenticationFilter filter) {
	     FilterRegistrationBean<JwtAuthenticationFilter> registration = new FilterRegistrationBean<>(filter);
	     registration.setEnabled(false);
	     return registration;
	 }
	 
}
