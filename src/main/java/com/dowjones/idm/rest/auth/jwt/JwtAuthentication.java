package com.dowjones.idm.rest.auth.jwt;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import lombok.Getter;

@Getter
public class JwtAuthentication implements Authentication {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Object principal;
	private Object credentials;	
	private Object details;
	private Collection<? extends GrantedAuthority> authorities;
	private String authToken;
	
	private boolean isAuthenticated;
	

	public JwtAuthentication(String authToken) {
		this.authToken = authToken;
		this.isAuthenticated = false;
	}
	
	public JwtAuthentication(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
		this.principal = principal;
		this.credentials = credentials;
		this.authorities = authorities;
		this.isAuthenticated = true;
	}

	@Override
	public Object getCredentials() {
		return credentials;
	}

	@Override
	public Object getPrincipal() {
		return principal;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public Object getDetails() {
		return details;
	}
	
	public void setDetails(Object details) {
		this.details = details;
	}

	@Override
	public boolean isAuthenticated() {
		return isAuthenticated;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		this.isAuthenticated = isAuthenticated;
	}
	

}
