package com.dowjones.idm.rest.auth.jwt.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AuthenticationResponse {

	private String jwtToken;
	
	public AuthenticationResponse(String token) {
		jwtToken = token;
	}
}
