package com.dowjones.idm.rest.auth.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.dowjones.idm.exception.InvalidJwtTokenException;

/**
 * @author amandeep
 *
 * Authentication provider access token validation.
 */
@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

	private static final String ACCESS_TOKEN_PREFIX = "Bearer ";
	
	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private JwtUtil jwtUtil;
	

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String tokenString = null;
		String subject = null;

		JwtAuthentication auth = (JwtAuthentication) authentication;

		String jwtToken = auth.getAuthToken();

		if(jwtToken != null && jwtToken.startsWith(ACCESS_TOKEN_PREFIX)) {
			tokenString = jwtToken.substring(7);
			try {
				subject = jwtUtil.extractUsername(tokenString);
				UserDetails userDetails = userDetailsService.loadUserByUsername(subject);
				jwtUtil.validateToken(tokenString, userDetails);
				return new JwtAuthentication(subject, null, userDetails.getAuthorities());
			} catch (InvalidJwtTokenException e) {
				throw e;
			}
		} else {
			throw new InvalidJwtTokenException("Access token missing or has invalid type.");
		}
	}
	

	@Override
	public boolean supports(Class<?> authentication) { 
		return JwtAuthentication.class == authentication;
	}

}
