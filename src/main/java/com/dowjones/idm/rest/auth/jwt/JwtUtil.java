package com.dowjones.idm.rest.auth.jwt;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.dowjones.idm.exception.InvalidJwtTokenException;
import com.dowjones.idm.rest.auth.jwt.config.JwtTokenConfig;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

/**
 * @author amandeep
 * 
 * Util class to generate and parse the jwt access token.
 *
 */
@Component
public class JwtUtil {

	@Autowired
	private JwtTokenConfig config;

	/**
	 * @param token
	 * @return
	 * @throws InvalidJwtTokenException
	 */
	private Claims extractClaims(String token) throws InvalidJwtTokenException {
		Claims claims = null;
		try {
			claims = Jwts.parser()
					.setSigningKey(config.getSecret())
					.parseClaimsJws(token)
					.getBody();
		} catch (ExpiredJwtException e) {
			throw new InvalidJwtTokenException(" Token expired for subject : " + e.getClaims().getSubject());
		} catch (SignatureException e) {
			throw new InvalidJwtTokenException(" Signature Invalid ");
		} catch(Exception ex) {
			throw new InvalidJwtTokenException(" Token Invalid ");
		}
		return claims;
	}

	/**
	 * @param token
	 * @return
	 */
	public String extractUsername(String token) {
		return extractClaims(token).getSubject();
	}


	/**
	 * @param userDetails
	 * @return
	 */
	public String generateToken(UserDetails userDetails) {
		String subject = userDetails.getUsername();
		return Jwts.builder()
				.setClaims(new HashMap<>())
				.setSubject(subject)
				.setIssuer(config.getJwtClaims().get("issuer"))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 3600000))
				.signWith(SignatureAlgorithm.HS256, config.getSecret())
				.compact();
	}

	/**
	 * @param token
	 * @param userDetails
	 * @throws InvalidJwtTokenException
	 */
	public void validateToken(String token, UserDetails userDetails) throws InvalidJwtTokenException {

		Claims claims = extractClaims(token);

		// Check if issuer is good.
		if(!config.getJwtClaims().get("issuer").equals(claims.getIssuer())) {
			throw new InvalidJwtTokenException(" Issuer Invalid. ");
		}


	}

}
