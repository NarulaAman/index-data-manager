package com.dowjones.idm.rest.auth.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.dowjones.idm.exception.InvalidJwtTokenException;
import com.dowjones.idm.rest.entity.Problem;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author amandeep
 * 
 * Authentication filter for JWT based authentication.
 * Validates the access token. 
 *
 */
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	@Autowired
	private AuthenticationManager manager;

	@Override
	public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		String authTokenHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        try {
		JwtAuthentication authResponse = (JwtAuthentication) manager.authenticate(new JwtAuthentication(authTokenHeader));
		authResponse.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
		SecurityContextHolder.getContext().setAuthentication(authResponse);
		chain.doFilter(request, response);
        } catch(InvalidJwtTokenException ex) {
            SecurityContextHolder.clearContext();
            handleUnsuccessfulAuthentication(response, ex);
        }
	}
	
	/**
	 * @param response
	 * @param ex
	 * @throws IOException
	 */
	private void handleUnsuccessfulAuthentication(HttpServletResponse response, InvalidJwtTokenException ex) throws IOException {
		
		Problem problem = new Problem();
		problem.setStatusCd(HttpServletResponse.SC_FORBIDDEN);
		problem.setType(ex.getType());
		problem.setDetail(ex.getMessage());
		
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		response.getWriter().write(new ObjectMapper().writeValueAsString(problem));
	}

}
