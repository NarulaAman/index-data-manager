package com.dowjones.idm.rest.auth.jwt.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * @author amandeep
 *
 * Config class that autowires JWT related properties.
 */
@Configuration
@ConfigurationProperties(prefix = "jwt")
@Setter
@Getter
public class JwtTokenConfig {

	private Map<String, String> jwtClaims;
	
	private String secret;
}
