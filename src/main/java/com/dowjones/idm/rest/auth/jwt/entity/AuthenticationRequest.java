package com.dowjones.idm.rest.auth.jwt.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AuthenticationRequest  {

	private String clientId;
	private String clientSecret;
	
}
