package com.dowjones.idm.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dowjones.idm.rest.entity.IndexData;
import com.dowjones.idm.rest.entity.IndexDataSet;
import com.dowjones.idm.service.IndexDataService;

/**
 * @author amandeep
 *
 * Rest controller for handling http index data requests.
 */
@RestController
@RequestMapping("/dowjones-index")
public class IndexDataApiController implements IndexDataApi {

	@Autowired
	private IndexDataService service;
	
	/**
	 *
	 */
	@PostMapping(value = "/records", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> uploadIndexDataSet(@RequestBody IndexDataSet dataSet) {
		service.uploadIndexDataSet(dataSet);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	/**
	 *
	 */
	@GetMapping(value = "/records/{ticker}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody IndexDataSet getIndexDataByTicker(@PathVariable("ticker") String ticker) {
		return service.getIndexDataByTicker(ticker);
	}

	/**
	 *
	 */
	@PostMapping(value = "/record", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> uploadIndexRecord(@RequestBody IndexData record) {
		service.uploadIndexRecord(record);	
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	} 
	
	
}
