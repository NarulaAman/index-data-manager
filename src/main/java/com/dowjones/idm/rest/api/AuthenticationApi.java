package com.dowjones.idm.rest.api;

import org.springframework.http.ResponseEntity;

import com.dowjones.idm.rest.auth.jwt.entity.AuthenticationRequest;

/**
 * @author amandeep
 *
 * Authentication API for JWT access token
 */
public interface AuthenticationApi {

	/**
	 * @param request
	 * @return
	 */
	public ResponseEntity<?> generateJwtToken(AuthenticationRequest request);
}
