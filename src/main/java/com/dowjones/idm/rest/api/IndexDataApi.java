package com.dowjones.idm.rest.api;

import org.springframework.http.ResponseEntity;

import com.dowjones.idm.rest.entity.IndexData;
import com.dowjones.idm.rest.entity.IndexDataSet;

/**
 * @author amandeep
 *
 * API for managing index data
 *
 */
public interface IndexDataApi {

	/**
	 * @param dataSet
	 */
	public ResponseEntity<Void> uploadIndexDataSet(IndexDataSet dataSet);
	/**
	 * @param ticker
	 * @return
	 */
	public IndexDataSet getIndexDataByTicker(String ticker);
	/**
	 * @param record
	 */
	public ResponseEntity<Void> uploadIndexRecord(IndexData record);
}
