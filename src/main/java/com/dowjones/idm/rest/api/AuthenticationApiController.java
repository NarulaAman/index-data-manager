package com.dowjones.idm.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dowjones.idm.exception.InvalidCredentialsException;
import com.dowjones.idm.rest.auth.jwt.JwtUtil;
import com.dowjones.idm.rest.auth.jwt.entity.AuthenticationRequest;
import com.dowjones.idm.rest.auth.jwt.entity.AuthenticationResponse;

/**
 * @author amandeep
 *
 * API Controller to handle authentication request and generate JWT access token.
 *
 */
@RestController
public class AuthenticationApiController implements AuthenticationApi {

	@Autowired
	private AuthenticationManager manager;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private JwtUtil jwtUtil;
	
	@PostMapping(value = "/authenticate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AuthenticationResponse> generateJwtToken(@org.springframework.web.bind.annotation.RequestBody AuthenticationRequest request) {
		try {
			manager.authenticate(new UsernamePasswordAuthenticationToken(request.getClientId(), request.getClientSecret()));
		} catch(AuthenticationException ex) {
			throw new InvalidCredentialsException("Invalid credentials.");
		}		
		UserDetails userDetails = userDetailsService.loadUserByUsername(request.getClientId());

		return ResponseEntity.ok(new AuthenticationResponse(jwtUtil.generateToken(userDetails)));
	}
}
