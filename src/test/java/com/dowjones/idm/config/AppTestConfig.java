package com.dowjones.idm.config;

import java.util.Arrays;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import com.dowjones.idm.service.IndexDataService;
import com.dowjones.idm.service.IndexDataServiceImpl;

@TestConfiguration
public class AppTestConfig {

	public IndexDataService indexDataService() {
		return new IndexDataServiceImpl();
	}

	@Bean
	@Primary
	public UserDetailsService userDetailsService() {
		User user = new User("testuser", "testuserpwd", Arrays.asList(new SimpleGrantedAuthority("ADMIN")));
		return new InMemoryUserDetailsManager(Arrays.asList(
				user
				));
	}
}
