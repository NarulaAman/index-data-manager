package com.dowjones.idm.rest.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.dowjones.idm.config.AppTestConfig;
import com.dowjones.idm.exception.TickerNotFoundException;
import com.dowjones.idm.repository.DataLoader;
import com.dowjones.idm.repository.IndexDataRepository;
import com.dowjones.idm.rest.auth.jwt.JwtAuthenticationFilter;
import com.dowjones.idm.rest.auth.jwt.JwtAuthenticationProvider;
import com.dowjones.idm.rest.auth.jwt.JwtUtil;
import com.dowjones.idm.rest.auth.jwt.config.JwtTokenConfig;
import com.dowjones.idm.rest.entity.IndexData;
import com.dowjones.idm.rest.entity.IndexDataSet;
import com.dowjones.idm.service.IndexDataService;
import com.dowjones.idm.util.Constant;

/**
 * @author amandeep
 *
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(IndexDataApiController.class)
@ContextConfiguration(classes = { AppTestConfig.class })
public class IndexDataApiControllerTest {
	
	private static final String STOCK_TICKER = "RBC";

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private DataLoader loader;
	
	@MockBean
	private IndexDataRepository repository;
	
	@SpyBean
	private JwtAuthenticationProvider provider;
	
	@SpyBean
	private JwtAuthenticationFilter filter;
	
	@SpyBean
	private JwtUtil jwtUtil;
	
	@SpyBean
	private JwtTokenConfig config;
	
	@MockBean
	private IndexDataService service;

	
	/**
	 * @throws Exception
	 */
	@Test
	@WithMockUser
	public void testUploadIndexDataSet() throws Exception {
		doNothing().when(service).uploadIndexDataSet(any(IndexDataSet.class));
		
		mvc.perform(post(Constant.API_CONTEXT_PATH + "records")
		        .header(HttpHeaders.AUTHORIZATION, getAuthorizationToken())
				.content("{\"dataSet\": [{\"stock\":\"RBC\"}]}")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
		        .andExpect(status().isCreated());
	}
	

	/**
	 * @throws Exception
	 */
	@Test
	@WithUserDetails("testuser")
	public void testUploadIndexRecord() throws Exception {
		doNothing().when(service).uploadIndexRecord(any(IndexData.class));
		
		mvc.perform(post(Constant.API_CONTEXT_PATH + "record")
				.content("{\"stock\":\"RBC\"}")
				.header(HttpHeaders.AUTHORIZATION, getAuthorizationToken())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
		        .andExpect(status().isCreated());
	}
	

	/**
	 * @throws Exception
	 */
	@Test
	public void testGetIndexDataByTicker() throws Exception {
		List<IndexData> listData = new ArrayList<>();
		IndexDataSet dataSet = new IndexDataSet();
		dataSet.setDataSet(listData);
		IndexData data = new IndexData();
		data.setStock(STOCK_TICKER);
		data.setQuarter(2);
		listData.add(data);
		
		doReturn(dataSet).when(service).getIndexDataByTicker(STOCK_TICKER);
		
		mvc.perform(get(Constant.API_CONTEXT_PATH + "records/RBC")
		        .header(HttpHeaders.AUTHORIZATION, getAuthorizationToken())
				.accept(MediaType.APPLICATION_JSON))
		        .andExpect(status().isOk())
		        .andExpect(jsonPath("$.dataSet[0].stock").value(STOCK_TICKER));
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void testGetIndexDataByTicker_NotFound() throws Exception {	
		doThrow(TickerNotFoundException.class).when(service).getIndexDataByTicker(STOCK_TICKER);
		
		mvc.perform(get(Constant.API_CONTEXT_PATH + "records/RBC")
		        .header(HttpHeaders.AUTHORIZATION, getAuthorizationToken())
				.accept(MediaType.APPLICATION_JSON))
		        .andExpect(status().isNotFound());
	}
	
	
	private String getAuthorizationToken() {		
		return "Bearer " + jwtUtil.generateToken(new User(Constant.TEST_USER, Constant.TEST_PWD, Arrays.asList(new SimpleGrantedAuthority("ADMIN"))));
	}
}
