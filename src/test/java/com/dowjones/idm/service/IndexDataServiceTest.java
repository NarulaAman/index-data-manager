package com.dowjones.idm.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;

import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.dowjones.idm.exception.TickerNotFoundException;
import com.dowjones.idm.repository.IndexDataRepository;
import com.dowjones.idm.rest.entity.IndexData;
import com.dowjones.idm.rest.entity.IndexDataSet;

/**
 * @author amandeep
 *
 */
@ExtendWith(SpringExtension.class)
public class IndexDataServiceTest {

	@SpyBean
	private IndexDataServiceImpl indexDataService;
	
	@MockBean
	private IndexDataRepository indexDataRepository;
	
	@Mock
	private IndexDataSet dataSet;
	@Mock
	private IndexData record;
	
	@BeforeEach
	public void init() {
		doReturn(Collections.EMPTY_LIST).when(dataSet).getDataSet();
	}
	
	@Test
	public void testUploadIndexDataSet() {
		doReturn(Collections.EMPTY_LIST).when(indexDataRepository).saveAll(anyList());
		indexDataService.uploadIndexDataSet(dataSet);
		verify(indexDataRepository, times(1)).saveAll(dataSet.getDataSet());
	}
	
	@Test
	public void testUploadIndexRecord() {
		doReturn(record).when(indexDataRepository).save(any(IndexData.class));
		indexDataService.uploadIndexRecord(record);
		verify(indexDataRepository, times(1)).save(record);
	}
	
	@Test
	public void testGetIndexDataByTicker() {
		doReturn(Arrays.asList(record)).when(indexDataRepository).getIndexDataByTicker(anyString());
		indexDataService.getIndexDataByTicker(Strings.EMPTY);
		verify(indexDataRepository, times(1)).getIndexDataByTicker(Strings.EMPTY);
	}
	
    @Test
	public void testGetIndexDataByTicker_NotFound() {
		doReturn(Collections.EMPTY_LIST).when(indexDataRepository).getIndexDataByTicker(anyString());
		assertThrows(TickerNotFoundException.class, () -> indexDataService.getIndexDataByTicker(Strings.EMPTY));
		verify(indexDataRepository, times(1)).getIndexDataByTicker(Strings.EMPTY);
		
	}
	
}
