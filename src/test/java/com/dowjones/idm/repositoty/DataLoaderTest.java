package com.dowjones.idm.repositoty;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.dowjones.idm.repository.DataLoader;
import com.dowjones.idm.rest.entity.IndexData;

/**
 * @author amandeep
 *
 */
@ExtendWith(SpringExtension.class)
public class DataLoaderTest {

	@Spy
	private DataLoader dataLoader;
	
	@Test
	public void testGetIndexData() {
		List<IndexData> dataSet = dataLoader.getIndexData();
		assertTrue(dataSet.size() == 750);
	}
	
	
	
}
