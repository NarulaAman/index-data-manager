# README #

### Background ###

* API to manage dow jones index data.
* Version 1.0.0
* API Specifications
  http://localhost:8080/swagger-ui/index.html#/

### Setup Instructions ###

* Simply clone the repository on your local machine
* If you want to connect to POSTGRES, you can install Docker on your machine to startup the container and run the microservice (Instruction Given Below)
  
### Deployment and Bootstrap Procedure ###

* Go to project root directory <br/>

1. Run ./mvnw spring-boot:run <br/>
   It connects to the in memory H2 Database. You can override the following arguments to run with another database server.<br/>
   SPRING_JPA_HIBERNATE_DDL_AUTO<br/>
   SPRING_DATASOURCE_URL<br/>
   SPRING_DATASOURCE_USERNAME<br/>
   SPRING_DATASOURCE_PASSWORD<br/>
   SPRING_DATABASE_PLATFORM<br/><br/>
   
2. Run the following commands <br/>
  ./mvnw clean install -U <br/>
  docker-compose up --build --force-recreate <br/>
  Runs the container that runs the microservice and connects to POSTGRES database. <br/>
  
  Application is deployed on your localhost and can be accessed at http://localhost:8080/dow-jones/<resource-path>.

### Contacts ###

* amand.narula@gmail.com 