FROM adoptopenjdk/openjdk11:alpine-jre

RUN mkdir /app

WORKDIR /app

COPY ./target/IndexDataManager*.jar ./indexdatamanager.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar", "indexdatamanager.jar"]